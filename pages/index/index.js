Page({
  // 数据
  data: {
    msg: "这是我的第一个小程序",
  },
  // 定义修改数据的方法
  changeMsg() {
    // 获取数据
    // this.data.msg = "this is my first mini-program";
    console.log(this.data.msg);

    // 数据变化, 视图更新
    // 数据是同步更新, 视图是异步更新
    // 如何知道视图更新完毕了呢?
    this.setData(
      {
        msg: "this is my first mini-program",
      },
      () => {
        console.log("视图更新完毕了");
      }
    );
  },
});
